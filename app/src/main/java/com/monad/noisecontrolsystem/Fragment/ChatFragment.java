package com.monad.noisecontrolsystem.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.monad.noisecontrolsystem.Adapter.ChatAdapter;
import com.monad.noisecontrolsystem.Model.MyData;
import com.monad.noisecontrolsystem.R;

import java.util.ArrayList;

public class ChatFragment extends Fragment implements View.OnClickListener
{
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<MyData> myDataset;
    private EditText editText;
    private Button post;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)  {
        View v = inflater.inflate(R.layout.fragment_3, container, false);

        editText = (EditText) v.findViewById(R.id.textView2);
        post = (Button) v.findViewById(R.id.button123);
        post.setOnClickListener(this);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.my_recycler_view);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        myDataset = new ArrayList<>();
        mAdapter = new ChatAdapter(myDataset);
        mRecyclerView.setAdapter(mAdapter);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.button123:
                String inPutText = editText.getText().toString();
                editText.setText("");
                myDataset.add(new MyData(inPutText));
                mAdapter.notifyDataSetChanged();
                break;
        }
    }
}